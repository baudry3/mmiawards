<?php

namespace App\Tests;

use App\Controller\GetCollegesParLieux;
use PHPUnit\Framework\TestCase;

class GetCollegesParLieuxTest extends TestCase
{
    private $url = 'http://127.0.0.1:8000/api/industrie_du_futur/';

    public function testCollegeByUaiReturnsJson()
    {
        $this->url .= '007/Annonay';
        $json_data = file_get_contents($this->url);
        $this->assertJson($json_data);
    }

    public function testStatusCodeIs200()
    {
        $this->url .= '007/Annonay';
        $headers = get_headers($this->url);
        $statusCode = substr($headers[0], 9, 3);
        $this->assertEquals(200, $statusCode);
    }

    public function testCollegeJsonContainsKeys()
    {
        $this->url .= '007/Annonay';
        $expectedKeys = ['uai', 'nom_de_l_etablissment', 'academie', 'departement', 'secteur'];
        $receivedJson = file_get_contents($this->url);
        $receivedData = json_decode($receivedJson, true);

        foreach ($expectedKeys as $key) {
            $this->assertArrayHasKey($key, $receivedData[0]);
        }
    }

    public function testDepartmentAndCity()
    {
        $dep = ['id' => '007', 'nom' => 'ARDECHE'];
        $this->url .= $dep['id'].'/Annonay';
        $receivedJson = file_get_contents($this->url);
        $receivedData = json_decode($receivedJson, true);

        foreach ($receivedData as $data) {
            $this->assertEquals($dep['nom'], $data['departement']);
        }
    }

    public function testCollegeCityInvalid()
    {
        $this->url .= '007/Invalid';
        $expectedJson = '[]';
        $receivedJson = file_get_contents($this->url);
        $this->assertJsonStringEqualsJsonString($expectedJson, $receivedJson);
    }

    public function testCollegeDepInvalid()
    {
        $this->url .= 'Invalid/Annonay';
        $expectedJson = '[]';
        $receivedJson = file_get_contents($this->url);
        $this->assertJsonStringEqualsJsonString($expectedJson, $receivedJson);
    }
    
    public function testStatusCodeIs404()
    {
        $this->url .= '';
        $headers = get_headers($this->url);
        $statusCode = substr($headers[0], 9, 3);
        $this->assertEquals(404, $statusCode);
    }

}
