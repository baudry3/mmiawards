<?php

namespace App\Test\Controller;

use App\Entity\DNB2018;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DNB2018ControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/d/n/b2018/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(DNB2018::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('DNB2018 index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'd_n_b2018[session]' => 'Testing',
            'd_n_b2018[numero_etablissement]' => 'Testing',
            'd_n_b2018[denomination_principale]' => 'Testing',
            'd_n_b2018[patronyme]' => 'Testing',
            'd_n_b2018[secteur_denseignement]' => 'Testing',
            'd_n_b2018[commune]' => 'Testing',
            'd_n_b2018[libelle_commune]' => 'Testing',
            'd_n_b2018[code_departement]' => 'Testing',
            'd_n_b2018[libelle_departement]' => 'Testing',
            'd_n_b2018[code_academie]' => 'Testing',
            'd_n_b2018[libelle_academie]' => 'Testing',
            'd_n_b2018[code_region]' => 'Testing',
            'd_n_b2018[libelle_region]' => 'Testing',
            'd_n_b2018[inscrits]' => 'Testing',
            'd_n_b2018[presents]' => 'Testing',
            'd_n_b2018[admis]' => 'Testing',
            'd_n_b2018[admis_sans_mention]' => 'Testing',
            'd_n_b2018[taux_de_reussite]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new DNB2018();
        $fixture->setSession('My Title');
        $fixture->setNumero_etablissement('My Title');
        $fixture->setDenomination_principale('My Title');
        $fixture->setPatronyme('My Title');
        $fixture->setSecteur_denseignement('My Title');
        $fixture->setCommune('My Title');
        $fixture->setLibelle_commune('My Title');
        $fixture->setCode_departement('My Title');
        $fixture->setLibelle_departement('My Title');
        $fixture->setCode_academie('My Title');
        $fixture->setLibelle_academie('My Title');
        $fixture->setCode_region('My Title');
        $fixture->setLibelle_region('My Title');
        $fixture->setInscrits('My Title');
        $fixture->setPresents('My Title');
        $fixture->setAdmis('My Title');
        $fixture->setAdmis_sans_mention('My Title');
        $fixture->setTaux_de_reussite('My Title');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('DNB2018');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new DNB2018();
        $fixture->setSession('Value');
        $fixture->setNumero_etablissement('Value');
        $fixture->setDenomination_principale('Value');
        $fixture->setPatronyme('Value');
        $fixture->setSecteur_denseignement('Value');
        $fixture->setCommune('Value');
        $fixture->setLibelle_commune('Value');
        $fixture->setCode_departement('Value');
        $fixture->setLibelle_departement('Value');
        $fixture->setCode_academie('Value');
        $fixture->setLibelle_academie('Value');
        $fixture->setCode_region('Value');
        $fixture->setLibelle_region('Value');
        $fixture->setInscrits('Value');
        $fixture->setPresents('Value');
        $fixture->setAdmis('Value');
        $fixture->setAdmis_sans_mention('Value');
        $fixture->setTaux_de_reussite('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'd_n_b2018[session]' => 'Something New',
            'd_n_b2018[numero_etablissement]' => 'Something New',
            'd_n_b2018[denomination_principale]' => 'Something New',
            'd_n_b2018[patronyme]' => 'Something New',
            'd_n_b2018[secteur_denseignement]' => 'Something New',
            'd_n_b2018[commune]' => 'Something New',
            'd_n_b2018[libelle_commune]' => 'Something New',
            'd_n_b2018[code_departement]' => 'Something New',
            'd_n_b2018[libelle_departement]' => 'Something New',
            'd_n_b2018[code_academie]' => 'Something New',
            'd_n_b2018[libelle_academie]' => 'Something New',
            'd_n_b2018[code_region]' => 'Something New',
            'd_n_b2018[libelle_region]' => 'Something New',
            'd_n_b2018[inscrits]' => 'Something New',
            'd_n_b2018[presents]' => 'Something New',
            'd_n_b2018[admis]' => 'Something New',
            'd_n_b2018[admis_sans_mention]' => 'Something New',
            'd_n_b2018[taux_de_reussite]' => 'Something New',
        ]);

        self::assertResponseRedirects('/d/n/b2018/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getSession());
        self::assertSame('Something New', $fixture[0]->getNumero_etablissement());
        self::assertSame('Something New', $fixture[0]->getDenomination_principale());
        self::assertSame('Something New', $fixture[0]->getPatronyme());
        self::assertSame('Something New', $fixture[0]->getSecteur_denseignement());
        self::assertSame('Something New', $fixture[0]->getCommune());
        self::assertSame('Something New', $fixture[0]->getLibelle_commune());
        self::assertSame('Something New', $fixture[0]->getCode_departement());
        self::assertSame('Something New', $fixture[0]->getLibelle_departement());
        self::assertSame('Something New', $fixture[0]->getCode_academie());
        self::assertSame('Something New', $fixture[0]->getLibelle_academie());
        self::assertSame('Something New', $fixture[0]->getCode_region());
        self::assertSame('Something New', $fixture[0]->getLibelle_region());
        self::assertSame('Something New', $fixture[0]->getInscrits());
        self::assertSame('Something New', $fixture[0]->getPresents());
        self::assertSame('Something New', $fixture[0]->getAdmis());
        self::assertSame('Something New', $fixture[0]->getAdmis_sans_mention());
        self::assertSame('Something New', $fixture[0]->getTaux_de_reussite());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new DNB2018();
        $fixture->setSession('Value');
        $fixture->setNumero_etablissement('Value');
        $fixture->setDenomination_principale('Value');
        $fixture->setPatronyme('Value');
        $fixture->setSecteur_denseignement('Value');
        $fixture->setCommune('Value');
        $fixture->setLibelle_commune('Value');
        $fixture->setCode_departement('Value');
        $fixture->setLibelle_departement('Value');
        $fixture->setCode_academie('Value');
        $fixture->setLibelle_academie('Value');
        $fixture->setCode_region('Value');
        $fixture->setLibelle_region('Value');
        $fixture->setInscrits('Value');
        $fixture->setPresents('Value');
        $fixture->setAdmis('Value');
        $fixture->setAdmis_sans_mention('Value');
        $fixture->setTaux_de_reussite('Value');

        $this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/d/n/b2018/');
        self::assertSame(0, $this->repository->count([]));
    }
}
