<?php

namespace App\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetDNB2018ParUAIControllerTest extends WebTestCase
{
    private $url = 'http://127.0.0.1:8000/api/getDNB2018result/';

    public function testDNBByUaiReturnsJson()
    {
        $this->url .= '0010022U';
        $json_data = file_get_contents($this->url);
        $this->assertJson($json_data);
    }

    public function testStatusCodeIs200()
    {
        $this->url .= '0010022U';
        $headers = get_headers($this->url);
        $statusCode = substr($headers[0], 9, 3);
        $this->assertEquals(200, $statusCode);
    }

    public function testDNBJsonStructure()
    {
        $this->url .= '0010022U';
        $expectedJson = '[{"taux_de_reussite":"88.8 %","admis_sans_mention":"24","admis":"87","presents":"98","inscrits":"99","session":2018}]';
        $receivedJson = file_get_contents($this->url);
        $this->assertJsonStringEqualsJsonString($expectedJson, $receivedJson);
    }

    public function testDNBJsonContainsKeys()
    {
        $this->url .= '0010022U';
        $expectedKeys = ['taux_de_reussite', 'admis_sans_mention', 'admis', 'presents', 'inscrits', 'session'];
        $receivedJson = file_get_contents($this->url);
        $receivedData = json_decode($receivedJson, true);

        foreach ($expectedKeys as $key) {
            $this->assertArrayHasKey($key, $receivedData[0]);
        }
    }

    public function testDNBValuesAreStringsExceptSession()
    {
        $this->url .= '0010022U';
        $receivedJson = file_get_contents($this->url);
        $receivedData = json_decode($receivedJson, true); // Convert JSON to associative array

        foreach ($receivedData[0] as $key => $value) {
            if ($key !== 'session') {
                $this->assertIsString($value);
            } else {
                $this->assertIsInt($value);
            }
        }
    }

    public function testDNBInvalid()
    {
        $this->url .= '0';
        $expectedJson = '[]';
        $receivedJson = file_get_contents($this->url);
        $this->assertJsonStringEqualsJsonString($expectedJson, $receivedJson);
    }

    public function testStatusCodeIs404()
    {
        $this->url .= '';
        $headers = get_headers($this->url);
        $statusCode = substr($headers[0], 9, 3);
        $this->assertEquals(404, $statusCode);
    }
}
