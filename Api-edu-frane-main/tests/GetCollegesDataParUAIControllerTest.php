<?php

namespace App\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetCollegesDataParUAIControllerTest extends WebTestCase
{
    private $url = 'http://127.0.0.1:8000/api/getCollegesUai/';

    public function testCollegeByUaiReturnsJson()
    {
        $this->url .= '0010022U';
        $json_data = file_get_contents($this->url);
        $this->assertJson($json_data);
    }

    public function testStatusCodeIs200()
    {
        $this->url .= '0010022U';
        $headers = get_headers($this->url);
        $statusCode = substr($headers[0], 9, 3);
        $this->assertEquals(200, $statusCode);
    }

    public function testCollegeJsonContainsKeys()
    {
        $this->url .= '0010022U';
        $expectedKeys = [
            'id', 'rentree_scolaire', 'academie', 'code_du_departement', 'departement', 'uai',
            'nom_de_l_etablissment', 'code_insee_de_la_commune', 'nom_de_la_commune', 'secteur',
            'ips', 'ecart_type_de_l_ips', 'rentreeScolaire', 'codeDuDepartement', 'nomDeLEtablissment',
            'codeInseeDeLaCommune', 'nomDeLaCommune', 'ecartTypeDeLIps'
        ];
        $receivedJson = file_get_contents($this->url);
        $receivedData = json_decode($receivedJson, true);

        foreach ($expectedKeys as $key) {
            $this->assertArrayHasKey($key, $receivedData[0]);
        }
    }

    public function testUaiMatches()
    {
        $uai = "0010022U";
        $this->url .= $uai;
        $receivedJson = file_get_contents($this->url);
        $receivedData = json_decode($receivedJson, true); // Convert JSON to associative array

        foreach ($receivedData as $data) {
            $this->assertEquals($uai, $data['uai']);
        }
    }

    public function testCollegeInvalid()
    {
        $this->url .= '0';
        $expectedJson = '[]';
        $receivedJson = file_get_contents($this->url);
        $this->assertJsonStringEqualsJsonString($expectedJson, $receivedJson);
    }

    public function testStatusCodeIs404()
    {
        $this->url .= '';
        $headers = get_headers($this->url);
        $statusCode = substr($headers[0], 9, 3);
        $this->assertEquals(404, $statusCode);
    }
}
