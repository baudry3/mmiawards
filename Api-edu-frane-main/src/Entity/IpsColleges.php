<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\GetCollegesDataParUAIController;
use App\Controller\GetCollegesParLieux;
use App\Repository\IpsCollegesRepository;

#[ORM\Entity(repositoryClass: IpsCollegesRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            uriTemplate: 'admin/ips_collegess'
        ),
        new Delete(
            uriTemplate: 'admin/ips_collegess/{id}'
        ),
        new Patch(
            uriTemplate: 'admin/ips_collegess/{id}'
        ),
        new Get(
            uriTemplate: 'user/ips_collegess/{id}'
        ),
        new GetCollection(
            uriTemplate: 'user/ips_collegess'
        ),
        new Get(
            uriTemplate: '/industrie_du_futur/{tagregion}/{ville}',
            requirements: [
                'tagregion' => '\d+',
                'ville' => '\w+',
            ],
            controller: GetCollegesParLieux::class,
            read: false,
            output: false,
            description: 'Récupère un MontantInvest par son type',
            name: 'get_by_title',

        ),
        new Get(
            uriTemplate: '/getCollegesUai/{id}',
            requirements: [
                'id' => '\d+',
            ],
            controller: GetCollegesDataParUAIController::class,
            read: false,
            output: false,
            description: 'Récupère les infos du colleges selon uai',
            name: 'get_by_uai',
        ),
    ]
)]
class IpsColleges
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 9)]
    private ?string $rentree_scolaire = null;

    #[ORM\Column(length: 20)]
    private ?string $academie = null;

    #[ORM\Column(length: 3)]
    private ?string $code_du_departement = null;

    #[ORM\Column(length: 25)]
    private ?string $departement = null;

    #[ORM\Column(length: 8)]
    private ?string $uai = null;

    #[ORM\Column(length: 255)]
    private ?string $nom_de_l_etablissment = null;

    #[ORM\Column(length: 10)]
    private ?string $code_insee_de_la_commune = null;

    #[ORM\Column(length: 40)]
    private ?string $nom_de_la_commune = null;

    #[ORM\Column(length: 20)]
    private ?string $secteur = null;

    #[ORM\Column]
    private ?float $ips = null;

    #[ORM\Column(nullable: true)]
    private ?float $ecart_type_de_l_ips = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentreeScolaire(): ?string
    {
        return $this->rentree_scolaire;
    }

    public function setRentreeScolaire(string $rentree_scolaire): static
    {
        $this->rentree_scolaire = $rentree_scolaire;

        return $this;
    }

    public function getAcademie(): ?string
    {
        return $this->academie;
    }

    public function setAcademie(string $academie): static
    {
        $this->academie = $academie;

        return $this;
    }

    public function getCodeDuDepartement(): ?string
    {
        return $this->code_du_departement;
    }

    public function setCodeDuDepartement(string $code_du_departement): static
    {
        $this->code_du_departement = $code_du_departement;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(string $departement): static
    {
        $this->departement = $departement;

        return $this;
    }

    public function getUai(): ?string
    {
        return $this->uai;
    }

    public function setUai(string $uai): static
    {
        $this->uai = $uai;

        return $this;
    }

    public function getNomDeLEtablissment(): ?string
    {
        return $this->nom_de_l_etablissment;
    }

    public function setNomDeLEtablissment(string $nom_de_l_etablissment): static
    {
        $this->nom_de_l_etablissment = $nom_de_l_etablissment;

        return $this;
    }

    public function getCodeInseeDeLaCommune(): ?string
    {
        return $this->code_insee_de_la_commune;
    }

    public function setCodeInseeDeLaCommune(string $code_insee_de_la_commune): static
    {
        $this->code_insee_de_la_commune = $code_insee_de_la_commune;

        return $this;
    }

    public function getNomDeLaCommune(): ?string
    {
        return $this->nom_de_la_commune;
    }

    public function setNomDeLaCommune(string $nom_de_la_commune): static
    {
        $this->nom_de_la_commune = $nom_de_la_commune;

        return $this;
    }

    public function getSecteur(): ?string
    {
        return $this->secteur;
    }

    public function setSecteur(string $secteur): static
    {
        $this->secteur = $secteur;

        return $this;
    }

    public function getIps(): ?float
    {
        return $this->ips;
    }

    public function setIps(float $ips): static
    {
        $this->ips = $ips;

        return $this;
    }

    public function getEcartTypeDeLIps(): ?float
    {
        return $this->ecart_type_de_l_ips;
    }

    public function setEcartTypeDeLIps(float $ecart_type_de_l_ips): static
    {
        $this->ecart_type_de_l_ips = $ecart_type_de_l_ips;

        return $this;
    }
}
