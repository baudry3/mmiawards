<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\GetDNB2018ParUAIController;
use App\Repository\DNB2018Repository;

#[ORM\Entity(repositoryClass: DNB2018Repository::class)]
#[ApiResource(
    operations: [
        new Post(
            uriTemplate: 'admin/dnb2018s'
        ),
        new Delete(
            uriTemplate: 'admin/dnb2018s/{id}'
        ),
        new Patch(
            uriTemplate: 'admin/dnb2018s/{id}'
        ),
        new Get(
            uriTemplate: 'user/dnb2018s/{id}'
        ),
        new GetCollection(
            uriTemplate: 'user/dnb2018s'
        ),
        new Get(
            uriTemplate: '/getDNB2018result/{id}',
            requirements: [
                'id' => '\d+',
            ],
            controller: GetDNB2018ParUAIController::class,
            read: false,
            output: false,
            description: 'Récupère les infos du des résultat du DNB 2018 par colleges selon uai',
            name: 'get_DNB_by_uai',
        ),
    ]
)]
class DNB2018
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $session = null;

    #[ORM\Column(length: 10)]
    private ?string $numero_etablissement = null;

    #[ORM\Column(length: 30)]
    private ?string $denomination_principale = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $patronyme = null;

    #[ORM\Column(length: 15)]
    private ?string $secteur_denseignement = null;

    #[ORM\Column(length: 5)]
    private ?string $commune = null;

    #[ORM\Column(length: 40)]
    private ?string $libelle_commune = null;

    #[ORM\Column(length: 3)]
    private ?string $code_departement = null;

    #[ORM\Column(length: 25)]
    private ?string $libelle_departement = null;

    #[ORM\Column]
    private ?int $code_academie = null;

    #[ORM\Column(length: 20)]
    private ?string $libelle_academie = null;

    #[ORM\Column]
    private ?int $code_region = null;

    #[ORM\Column(length: 30)]
    private ?string $libelle_region = null;

    #[ORM\Column(length: 3)]
    private ?string $inscrits = null;

    #[ORM\Column(length: 3)]
    private ?string $presents = null;

    #[ORM\Column(length: 3)]
    private ?string $admis = null;

    #[ORM\Column(length: 3)]
    private ?string $admis_sans_mention = null;

    #[ORM\Column(length: 10)]
    private ?string $taux_de_reussite = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSession(): ?int
    {
        return $this->session;
    }

    public function setSession(int $session): static
    {
        $this->session = $session;

        return $this;
    }

    public function getNumeroEtablissement(): ?string
    {
        return $this->numero_etablissement;
    }

    public function setNumeroEtablissement(string $numero_etablissement): static
    {
        $this->numero_etablissement = $numero_etablissement;

        return $this;
    }

    public function getDenominationPrincipale(): ?string
    {
        return $this->denomination_principale;
    }

    public function setDenominationPrincipale(string $denomination_principale): static
    {
        $this->denomination_principale = $denomination_principale;

        return $this;
    }

    public function getPatronyme(): ?string
    {
        return $this->patronyme;
    }

    public function setPatronyme(?string $patronyme): static
    {
        $this->patronyme = $patronyme;

        return $this;
    }

    public function getSecteurDenseignement(): ?string
    {
        return $this->secteur_denseignement;
    }

    public function setSecteurDenseignement(string $secteur_denseignement): static
    {
        $this->secteur_denseignement = $secteur_denseignement;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(string $commune): static
    {
        $this->commune = $commune;

        return $this;
    }

    public function getLibelleCommune(): ?string
    {
        return $this->libelle_commune;
    }

    public function setLibelleCommune(string $libelle_commune): static
    {
        $this->libelle_commune = $libelle_commune;

        return $this;
    }

    public function getCodeDepartement(): ?string
    {
        return $this->code_departement;
    }

    public function setCodeDepartement(string $code_departement): static
    {
        $this->code_departement = $code_departement;

        return $this;
    }

    public function getLibelleDepartement(): ?string
    {
        return $this->libelle_departement;
    }

    public function setLibelleDepartement(string $libelle_departement): static
    {
        $this->libelle_departement = $libelle_departement;

        return $this;
    }

    public function getCodeAcademie(): ?int
    {
        return $this->code_academie;
    }

    public function setCodeAcademie(int $code_academie): static
    {
        $this->code_academie = $code_academie;

        return $this;
    }

    public function getLibelleAcademie(): ?string
    {
        return $this->libelle_academie;
    }

    public function setLibelleAcademie(string $libelle_academie): static
    {
        $this->libelle_academie = $libelle_academie;

        return $this;
    }

    public function getCodeRegion(): ?int
    {
        return $this->code_region;
    }

    public function setCodeRegion(int $code_region): static
    {
        $this->code_region = $code_region;

        return $this;
    }

    public function getLibelleRegion(): ?string
    {
        return $this->libelle_region;
    }

    public function setLibelleRegion(string $libelle_region): static
    {
        $this->libelle_region = $libelle_region;

        return $this;
    }

    public function getInscrits(): ?string
    {
        return $this->inscrits;
    }

    public function setInscrits(string $inscrits): static
    {
        $this->inscrits = $inscrits;

        return $this;
    }

    public function getPresents(): ?string
    {
        return $this->presents;
    }

    public function setPresents(string $presents): static
    {
        $this->presents = $presents;

        return $this;
    }

    public function getAdmis(): ?string
    {
        return $this->admis;
    }

    public function setAdmis(string $admis): static
    {
        $this->admis = $admis;

        return $this;
    }

    public function getAdmisSansMention(): ?string
    {
        return $this->admis_sans_mention;
    }

    public function setAdmisSansMention(string $admis_sans_mention): static
    {
        $this->admis_sans_mention = $admis_sans_mention;

        return $this;
    }

    public function getTauxDeReussite(): ?string
    {
        return $this->taux_de_reussite;
    }

    public function setTauxDeReussite(string $taux_de_reussite): static
    {
        $this->taux_de_reussite = $taux_de_reussite;

        return $this;
    }
}
