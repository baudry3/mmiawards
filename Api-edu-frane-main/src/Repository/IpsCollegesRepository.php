<?php

namespace App\Repository;

use App\Entity\IpsColleges;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IpsColleges>
 *
 * @method IpsColleges|null find($id, $lockMode = null, $lockVersion = null)
 * @method IpsColleges|null findOneBy(array $criteria, array $orderBy = null)
 * @method IpsColleges[]    findAll()
 * @method IpsColleges[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IpsCollegesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IpsColleges::class);
    }

    public function GetCollegesParLieux($numRegion, $nomVille): array
    {
        return $this->createQueryBuilder('i')
            ->select('DISTINCT i.uai, i.nom_de_l_etablissment, i.academie, i.departement, i.secteur')
            ->andWhere('i.code_du_departement = :numRegion')
            ->andWhere('i.nom_de_la_commune = :nomVille')
            ->setParameter('numRegion', $numRegion)
            ->setParameter('nomVille', $nomVille)
            ->getQuery()
            ->getResult();
    }

    public function GetCollegesParUAI($uai): array
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.uai = :uai')
            ->setParameter('uai', $uai)
            ->getQuery()
            ->getResult();
    }

    //    /**
    //     * @return IpsColleges[] Returns an array of IpsColleges objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('i.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?IpsColleges
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
