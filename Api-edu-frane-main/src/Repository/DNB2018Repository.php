<?php

namespace App\Repository;

use App\Entity\DNB2018;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DNB2018>
 *
 * @method DNB2018|null find($id, $lockMode = null, $lockVersion = null)
 * @method DNB2018|null findOneBy(array $criteria, array $orderBy = null)
 * @method DNB2018[]    findAll()
 * @method DNB2018[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DNB2018Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DNB2018::class);
    }


    public function GetDNBByUai($uai): array
    {
        return $this->createQueryBuilder('d')
            ->select('d.taux_de_reussite, d.admis_sans_mention, d.admis, d.presents, d.inscrits, d.taux_de_reussite, d.session')
            ->andWhere('d.numero_etablissement = :uai')
            ->setParameter('uai', $uai)
            ->getQuery()
            ->getResult();
    }

    //    /**
    //     * @return DNB2018[] Returns an array of DNB2018 objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DNB2018
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
