<?php

namespace App\Controller;

use App\Repository\DNB2018Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GetDNB2018ParUAIController extends AbstractController
{
    private $repository;

    public function __construct(DNB2018Repository $repository)
    {
        $this->repository = $repository;
    }

    // /industrie_du_futur/{tagregion}/{ville}
    #[Route('/api/getDNB2018result/{id}', methods: ['GET'])]
    public function __invoke($id)
    {
        $montantInvest = $this->repository->GetDNBByUai($id);

        return $this->json($montantInvest);
    }
}
