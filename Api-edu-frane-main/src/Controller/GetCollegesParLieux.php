<?php

namespace App\Controller;

use App\Repository\IpsCollegesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GetCollegesParLieux extends AbstractController
{
    private $repository;

    public function __construct(IpsCollegesRepository $repository)
    {
        $this->repository = $repository;
    }

    // /industrie_du_futur/{tagregion}/{ville}
    #[Route('/api/industrie_du_futur/{tagregion}/{ville}', methods: ['GET'])]
    public function __invoke($tagregion, $ville)
    {
        // $title = $id;

        // $montantInvest = $this->repository->findBy(['code_departement' => $title]);

        // if (!$montantInvest) {
        //     return $this->json(['message' => 'code_departement not found'], 404);
        // }

        $montantInvest = $this->repository->GetCollegesParLieux($tagregion, $ville);

        return $this->json($montantInvest);
    }
}
