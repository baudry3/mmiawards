<?php

namespace App\Controller;

use App\Repository\IpsCollegesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GetCollegesDataParUAIController extends AbstractController
{
    private $repository;

    public function __construct(IpsCollegesRepository $repository)
    {
        $this->repository = $repository;
    }

    // /industrie_du_futur/{tagregion}/{ville}
    #[Route('/api/getCollegesUai/{id}', methods: ['GET'])]
    public function __invoke($id)
    {
        $montantInvest = $this->repository->GetCollegesParUAI($id);

        return $this->json($montantInvest);
    }
}
