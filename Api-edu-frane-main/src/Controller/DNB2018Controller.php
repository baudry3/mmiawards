<?php

namespace App\Controller;

use App\Entity\DNB2018;
use App\Form\DNB2018Type;
use App\Repository\DNB2018Repository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
#[Route('/dnb2018')]
class DNB2018Controller extends AbstractController
{
    #[Route('/', name: 'app_d_n_b2018_index', methods: ['GET'])]
    public function index(
        DNB2018Repository $dNB2018Repository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        if ($request->query->has('search') && $request->query->get('search') != '') {
            $donnees = $dNB2018Repository->findBy(
                ['libelle_commune' => strtoupper($request->query->get('search'))]
            );
        } else {
            $donnees = $dNB2018Repository->findAll();
        }

        $pagination = $paginator->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            15
        );

        return $this->render('dnb2018/index.html.twig', [
            'dnb2018s' => $pagination
        ]);
    }

    #[Route('/new', name: 'app_d_n_b2018_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $dNB2018 = new DNB2018();
        $form = $this->createForm(DNB2018Type::class, $dNB2018);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($dNB2018);
            $entityManager->flush();

            return $this->redirectToRoute('app_d_n_b2018_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('dnb2018/new.html.twig', [
            'd_n_b2018' => $dNB2018,
            'form' => $form,
            'type' => 'new'
        ]);
    }

    #[Route('/{id}', name: 'app_d_n_b2018_show', methods: ['GET'])]
    public function show(DNB2018 $dNB2018): Response
    {
        return $this->render('dnb2018/show.html.twig', [
            'd_n_b2018' => $dNB2018,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_d_n_b2018_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, DNB2018 $dNB2018, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DNB2018Type::class, $dNB2018);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_d_n_b2018_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('dnb2018/new.html.twig', [
            'd_n_b2018' => $dNB2018,
            'form' => $form,
            'type' => 'edit'
        ]);
    }

    #[Route('/{id}', name: 'app_d_n_b2018_delete', methods: ['POST'])]
    public function delete(Request $request, DNB2018 $dNB2018, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $dNB2018->getId(), $request->request->get('_token'))) {
            $entityManager->remove($dNB2018);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_d_n_b2018_index', [], Response::HTTP_SEE_OTHER);
    }
}
