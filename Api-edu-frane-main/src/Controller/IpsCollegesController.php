<?php

namespace App\Controller;

use App\Entity\IpsColleges;
use App\Form\IpsCollegesType;
use App\Repository\IpsCollegesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Knp\Component\Pager\PaginatorInterface;

#[IsGranted('ROLE_ADMIN')]
#[Route('/ips')]
class IpsCollegesController extends AbstractController
{
    #[Route('/', name: 'app_ips_colleges_index', methods: ['GET'])]
    public function index(
        IpsCollegesRepository $ipsCollegesRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        if ($request->query->has('search') && $request->query->get('search') != '') {
            $donnees = $ipsCollegesRepository->findBy(
                ['nom_de_la_commune' => strtoupper($request->query->get('search'))]
            );
        } else {
            $donnees = $ipsCollegesRepository->findAll();
        }

        $pagination = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer
            $request->query->getInt('page', 1), // Numéro de page à afficher, 1 par défaut
            15 // Nombre d'éléments par page
        );

        return $this->render('ips_colleges/index.html.twig', [
            'ips_colleges' => $pagination,
        ]);
    }

    #[Route('/new', name: 'app_ips_colleges_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {


        $ipsCollege = new IpsColleges();
        $form = $this->createForm(IpsCollegesType::class, $ipsCollege);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ipsCollege);
            $entityManager->flush();

            return $this->redirectToRoute('app_ips_colleges_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('ips_colleges/new.html.twig', [
            'ips_college' => $ipsCollege,
            'form' => $form,
            'type' => 'new'
        ]);
    }

    #[Route('/{id}/edit', name: 'app_ips_colleges_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, IpsColleges $ipsCollege, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(IpsCollegesType::class, $ipsCollege);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_ips_colleges_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('ips_colleges/new.html.twig', [
            'ips_college' => $ipsCollege,
            'form' => $form,
            'type' => 'edit'
        ]);
    }

    #[Route('/{id}', name: 'app_ips_colleges_delete', methods: ['POST'])]
    public function delete(Request $request, IpsColleges $ipsCollege, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $ipsCollege->getId(), $request->request->get('_token'))) {
            $entityManager->remove($ipsCollege);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_ips_colleges_index', [], Response::HTTP_SEE_OTHER);
    }
}
