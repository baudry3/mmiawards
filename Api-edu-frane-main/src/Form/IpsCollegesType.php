<?php

namespace App\Form;

use App\Entity\IpsColleges;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IpsCollegesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('rentree_scolaire', TextType::class, ['attr' => ['maxlength' => 9]])
            ->add('academie', TextType::class, ['attr' => ['maxlength' => 20]])
            ->add('code_du_departement', TextType::class, ['attr' => ['maxlength' => 3]])
            ->add('departement', TextType::class, ['attr' => ['maxlength' => 25]])
            ->add('uai', TextType::class, ['attr' => ['maxlength' => 8]])
            ->add('nom_de_l_etablissment', TextType::class, ['attr' => ['maxlength' => 255]])
            ->add('code_insee_de_la_commune', TextType::class, ['attr' => ['maxlength' => 10]])
            ->add('nom_de_la_commune', TextType::class, ['attr' => ['maxlength' => 40]])
            ->add('secteur', TextType::class, ['attr' => ['maxlength' => 20]])
            ->add('ips', NumberType::class)
            ->add('ecart_type_de_l_ips', NumberType::class, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IpsColleges::class,
        ]);
    }
}
