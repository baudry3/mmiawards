<?php

namespace App\Form;

use App\Entity\DNB2018;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DNB2018Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('session')
            ->add('numero_etablissement')
            ->add('denomination_principale')
            ->add('patronyme')
            ->add('secteur_denseignement')
            ->add('commune')
            ->add('libelle_commune')
            ->add('code_departement')
            ->add('libelle_departement')
            ->add('code_academie')
            ->add('libelle_academie')
            ->add('code_region')
            ->add('libelle_region')
            ->add('inscrits')
            ->add('presents')
            ->add('admis')
            ->add('admis_sans_mention')
            ->add('taux_de_reussite')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DNB2018::class,
        ]);
    }
}
