<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240221101545 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE dnb2018 (id INT AUTO_INCREMENT NOT NULL, session INT NOT NULL, numero_etablissement VARCHAR(10) NOT NULL, denomination_principale VARCHAR(30) NOT NULL, patronyme VARCHAR(255) DEFAULT NULL, secteur_denseignement VARCHAR(15) NOT NULL, commune VARCHAR(5) NOT NULL, libelle_commune VARCHAR(40) NOT NULL, code_departement VARCHAR(3) NOT NULL, libelle_departement VARCHAR(25) NOT NULL, code_academie INT NOT NULL, libelle_academie VARCHAR(20) NOT NULL, code_region INT NOT NULL, libelle_region VARCHAR(30) NOT NULL, inscrits VARCHAR(3) NOT NULL, presents VARCHAR(3) NOT NULL, admis VARCHAR(3) NOT NULL, admis_sans_mention VARCHAR(3) NOT NULL, taux_de_reussite VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE dnb2018');
    }
}
