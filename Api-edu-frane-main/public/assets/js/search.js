
console.log('search.js');

document.addEventListener("DOMContentLoaded", function () {
    const urlParams = new URLSearchParams(window.location.search);
    var searchInput = document.querySelector("input[name='search']");
    var searchButton = document.querySelector("button[type='submit']");

    if (urlParams.has('search') && urlParams.get('search') != '') {
        searchButton.innerText = 'Réinitialiser';
        searchInput.addEventListener('input', function () {
            if (searchInput.value != '') {
                searchButton.innerText = 'Rechercher';
            } else {
                searchButton.innerText = 'Réinitialiser';
            }
        });
    } else {

    }

});