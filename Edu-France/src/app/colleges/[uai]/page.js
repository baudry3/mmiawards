"use client";


import { useEffect, useState, } from "react";
import { useParams } from "next/navigation";

import FileAriane from '../../model/FileAriane';
import DataTable from '../../model/DataTable';
import ListCollege from '../../model/ListCollege';


import './../../assets/style/uaiPage.scss';

export default function UaiPage() {
    const params = useParams();
    const [listCollege, setListCollege] = useState([]);
    const [dataCollege, setDataCollege] = useState();
    const [nomDeLetablissment, setNomDeLetablissment] = useState('Chargement');
    const [infoOpenData, setInfoOpenData] = useState([]);
    const [infoColleges, setInfoColleges] = useState([]);

    useEffect(() => {
        getResult()
        getCollege()
    }, []);

    const getResult = () => {
        fetch(`https://api-edu-college.baudrysouvignet.fr/api/getDNB2018result/${params.uai}`)
            .then(response => response.json())
            .then((dnbData) => {
                setInfoOpenData([
                    {
                        number: dnbData[0].taux_de_reussite.replace(/\s/g, ""),
                        description: "Taux de réussite"
                    },
                    {
                        number: dnbData[0].inscrits.replace(/\s/g, ""),
                        description: "Inscrits"
                    },
                    {
                        number: dnbData[0].presents.replace(/\s/g, ""),
                        description: "Présent"
                    },
                    {
                        number: dnbData[0].admis.replace(/\s/g, ""),
                        description: "Admis"
                    },
                    {
                        number: dnbData[0].admis_sans_mention.replace(/\s/g, ""),
                        description: "Sans mention"
                    }
                    ,
                    {
                        number: dnbData[0].session,
                        description: "Session"
                    }
                ]);

            })
    }

    const getCollege = () => {
        fetch(`https://api-edu-college.baudrysouvignet.fr/api/getCollegesUai/${params.uai}`)
            .then(response => response.json())
            .then((data) => {
                const dataCollege = [...data].sort((a, b) => b.rentree_scolaire.localeCompare(a.rentree_scolaire)).slice(0, 4);
                setDataCollege(dataCollege);
                setNomDeLetablissment(dataCollege[0].nom_de_l_etablissment);
                getListOfCollege(dataCollege);

                const infoCollege = [];
                dataCollege.forEach(element => {
                    infoCollege.push({
                        number: element.ips,
                        description: element.rentree_scolaire,
                    });
                });
                infoCollege.push({
                    number: "102.7",
                    description: "Moyenne"
                },
                    {
                        number: "16.6",
                        description: "Ecart-Type"
                    })
                setInfoColleges(infoCollege);
            })
    }

    const getListOfCollege = (dataCollege) => {
        fetch(`https://api-edu-college.baudrysouvignet.fr/api/industrie_du_futur/${dataCollege[0].code_du_departement}/${encodeURIComponent(dataCollege[0].nom_de_la_commune)}`)
            .then(response => response.json())
            .then((data) => {
                setListCollege(data.filter(item => item.uai !== dataCollege[0].uai));
            })
    }

    const fileAriane = [
        {
            href: "/",
            children: "Accueil"
        },
        {
            href: dataCollege ? `/${dataCollege[0].code_du_departement}/${encodeURIComponent(dataCollege[0].nom_de_la_commune)}` : "/",
            children: dataCollege ? `${dataCollege[0].nom_de_la_commune} (${dataCollege[0].code_du_departement})` : "Chargement"
        },
        {
            href: "#",
            children: nomDeLetablissment
        }
    ];


    return (
        <>
            <FileAriane data={fileAriane} />
            <h1>{nomDeLetablissment}</h1>

            <section className="info">
            <article>
                    {infoOpenData.length !== 0 ? (
                        <>
                            <h2>Résultat brevet 2018</h2>
                            <DataTable datas={infoOpenData} />
                        </>
                    ) : ""}
                </article>
                <article>
                    {infoColleges.length !== 0 ? (
                        <>
                            <h2>Indice de position sociale</h2>
                            <DataTable datas={infoColleges} />
                        </>
                    ) : ""}
                </article>
            </section>

            {listCollege.length > 0 && (
                <section>
                    <h2>Les autres colléges de votre région</h2>

                    <div className="contenu">
                        {listCollege.map((item, index) => (
                            <ListCollege item={item} key={index} />
                        ))}
                    </div>
                </section>
            )}
        </>
    );
}
