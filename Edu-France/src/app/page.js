"use client";

import FileAriane from './model/FileAriane';
import SectionInfo from './model/SectionInfo';
import DataListCard from './components/CardInfo';
import DataTable from './model/DataTable';
import FormSearchCollege from './model/FormSearchCollege';


import './assets/style/home.scss';

export default function Home() {
  var fileAriane = [
    {
      href: "/",
      children: "Accueil"
    }
  ];

  var infoOpenData = [
    {
      number: "8825",
      description: "Collèges"
    },
    {
      number: "41701",
      description: "Données sur l'IPS"
    },
    {
      number: "02",
      description: "Jeux données"
    },
    {
      number: "2024",
      description: "Date mise à jour"
    }
  ]
  return (
    <>
      <FileAriane data={fileAriane}></FileAriane>

      <h1>Édu-France,</h1>
      <p>La plateforme dédiée à la transparence et à l'équité dans l'accès à l'information sur les collèges en France. Explorez nos données actualisées sur les résultats du Diplôme National du Brevet et les indices de position sociale.</p>

      <section className="form-section">
        <h2>Rechercher les établissement de votre ville</h2>
        <p>Recherchez les établissements scolaires dans votre ville en utilisant le formulaire ci-dessous</p>
        <p className="secondary">Sauf mention contraire, tous les champs sont obligatoires.</p>
        <FormSearchCollege />
      </section>

      <section className="info">
        <SectionInfo
          title="Jeux de données utilisé par la plateforme "
          lien={{ href: "https://www.data.gouv.fr/fr/", children: "Disponible sur data.gouv.fr" }}
        >
          <DataListCard href="https://www.data.gouv.fr/fr/datasets/diplome-national-du-brevet-par-etablissement/" headTitle="Données relatives aux" title="Diplôme National du Brevet" />
          <DataListCard href="https://www.data.gouv.fr/fr/datasets/indices-de-position-sociale-dans-les-colleges-de-france-metropolitaine-et-drom/" headTitle="Données relatives à" title="L’Indices de Position Sociale" />
        </SectionInfo>

        <SectionInfo
          title="Les données Édu-France c’est"
          lien={{ href: "https://www.data.gouv.fr/fr/", children: "Disponible sur data.gouv.fr" }}
        >
          <DataTable datas={infoOpenData}></DataTable>
        </SectionInfo>
      </section>
    </>
  );
}
