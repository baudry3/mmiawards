import { Inter } from "next/font/google";

import Header from './model/Header';

import "./globals.css";
import './assets/style/app.scss';
import Footer from "./model/Footer";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Èdu-France",
  description: "Obtenez les informations sur les colléges de france",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body  className={inter.className}>
        <Header baseline="Obtenez les informations sur les colléges de france"></Header>
        <main>{children}</main>
        <Footer></Footer>
      </body>
    </html>
  );
}
