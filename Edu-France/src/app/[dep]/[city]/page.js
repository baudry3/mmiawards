"use client";


import FileAriane from '../../model/FileAriane';
import { useParams } from "next/navigation";

import { useEffect, useState, } from "react";
import FormSearchCollege from '../../model/FormSearchCollege';
import ListCollege from '../../model/ListCollege';
import SelectField from '../../components/SelectField';
import TextField from '../../components/TextField';

import './../../assets/style/search.scss';

export default function Home() {
    const params = useParams();
    const [data, setData] = useState();
    const [originalData, setOriginalData] = useState();
    const [filterValue, setfilterValue] = useState();

    var fileAriane = [
        {
            href: "/",
            children: "Accueil"
        },
        {
            href: "/",
            children: decodeURIComponent(params.city) + " (" + params.dep + ")"
        }
    ];

    var dataForm = [
        { dep: params.dep, city: decodeURIComponent(params.city) }
    ];

    const filterData = [
        { value: "ASC", label: "Ordre Alphabtique" },
        { value: "ASCUai", label: "UAI croissant" },
        { value: "DESCUai", label: "UAI décroissant" }
    ];

    useEffect(() => {
        fetch(`https://api-edu-college.baudrysouvignet.fr/api/industrie_du_futur/${params.dep}/${params.city}`)
            .then((response) => response.json())
            .then((data) => {
                setData(data);
                setOriginalData(data);
            });
    }, []);


    // FILTRE DE LA SELECTION
    const filter = (data, filterValue) => {
        let sortedData = [];

        switch (filterValue) {
            case "ASC":
                sortedData = [...data].sort((a, b) => a.nom_de_l_etablissment.localeCompare(b.nom_de_l_etablissment));
                break;
            case "ASCUai":
                sortedData = [...data].sort((a, b) => a.uai.localeCompare(b.uai));
                break;
            case "DESCUai":
                sortedData = [...data].sort((a, b) => b.uai.localeCompare(a.uai));
                break;
            default:
                sortedData = data;
        }

        setfilterValue(filterValue);
        return sortedData;
    }

    const applyFilter = (event) => {
        let filterValue = event.target.value;
        setData(filter(data, filterValue));
    };

    // RECHERCHE DE LA SELECTION

    const search = (event) => {
        let searchValue = event.target.value.toLowerCase();

        if (searchValue === "") {
            let filteredData = filter(originalData, filterValue);
            setData(filteredData);
        } else {
            let filteredData = originalData.filter(item =>
                item.nom_de_l_etablissment.toLowerCase().includes(searchValue)
            );

            filteredData = filter(filteredData, filterValue);
            setData(filteredData);
        }
    };





    return (
        <>
            <FileAriane data={fileAriane}></FileAriane>

            <section className='search'>
                <h1>Votre Recherche</h1>
                <p>Recherche dans la ville de {decodeURIComponent(params.city)} ({params.dep})</p>
                <FormSearchCollege dataForPlaceholder={dataForm} />
            </section>

            <section className='data'>
                <aside>
                    <h2>Filtrer</h2>
                    <SelectField label="Trier" name="filter" dataSelect={filterData} placeholder="Trier par" onChange={applyFilter} />
                    <TextField label="Rechercher" name="search" placeholder="Rechercher par nom" onChange={search} />
                </aside>
                <article>
                    <p>{data ? data.length : 0} résultat(s)</p>
                    {data ? (
                        data.length === 0 ? <p>Aucun résultat</p> :
                            data.map((item, index) => (
                                <ListCollege item={item} key={index} />
                            ))

                    ) : (
                        <p>Chargement des données</p>
                    )}
                </article>
            </section>

        </>
    );
}
