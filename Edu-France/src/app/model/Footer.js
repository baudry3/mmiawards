'use client';

import React from 'react';

import Image from "next/image";

import Link from "next/link";
import Button from './../components/Button';

import './../assets/style/model/Footer.scss';
import { useState, useEffect } from "react";

const Footer = ({ children }) => {
    function openLink() {
        window.open('https://api-edu-college.baudrysouvignet.fr/', '_blank');
    }

    const [imageUrl, setImageUrl] = useState("/images/logo.svg");

    useEffect(() => {
        const detectPreferredTheme = () => {
            if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                setImageUrl("/images/logo-dark.svg");
            } else {
                setImageUrl("/images/logo.svg");
            }
        };

        detectPreferredTheme();

        const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
        const handleChange = () => {
            detectPreferredTheme();
        };
        mediaQuery.addListener(handleChange);

        return () => {
            mediaQuery.removeListener(handleChange);
        };
    }, []);

    return (
        <footer>
            <div className='text'>
                <Link href="/"><Image src="/images/newsroom-icon.svg" alt="logo" width={71} height={55} /></Link>
                <div className='text'>
                    <h2>Profiter de ces données pour vos projets !</h2>
                    <p>Vous shouaitez utiliser les données public de data.gouv pour votre projet concernant l’éducation inscrivez vous gratuitement à notre API</p>
                    <Button functionOnClick={openLink} type="main">S'inscrire</Button>
                </div>
            </div>
            <Image src={imageUrl} alt="logo" width={109} height={114} className='logo' style={{ width: "auto", height: "auto" }} />
        </footer >
    );
};

export default Footer;
