import React from 'react';


import './../assets/style/model/SectionInfo.scss';

import Lien from './../components/Lien';
import Image from "next/image";

const SectionInfo = ({ title, lien, children }) => {
    return (
        <article className='sectionInfo'>
            <div className='title'>
                <h2>{title}</h2>
                <Lien href={lien.href} type="main" openInBlank={true}>{lien.children}</Lien>
            </div>

            <div className='content'>
                {children}
            </div>
        </article>
    );
};

export default SectionInfo;
