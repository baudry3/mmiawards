import React from 'react';


import './../assets/style/model/ListCollege.scss';

const ListCollege = ({ item }) => {
    return (
        <div className='college' onClick={() => window.location.href = `/colleges/${item.uai}`}>
            <div className='title'>
                <h3>{item.nom_de_l_etablissment}</h3>
                <p>{item.academie} - {item.departement}</p>
            </div>
            <div className='tag'>
                <span className='uai'>{item.uai}</span>
                <span className='secteur'>{item.secteur}</span>
            </div>
        </div>
    );
};

export default ListCollege;
