import React from 'react';
import Button from '../components/Button';
import TextField from '../components/TextField';
import SelectField from '../components/SelectField';

const FormSearchCollege = ({ dataForPlaceholder }) => {
    const handleSubmit = (event) => {
        event.preventDefault();
        const city = event.target.city.value;
        const dep = event.target.dep.value;
        const destinationURL = `/${dep}/${city}`;
        window.location.href = destinationURL;
    };

    const depData = [
        { value: "001", label: "AIN" },
        { value: "002", label: "AISNE" },
        { value: "003", label: "ALLIER" },
        { value: "004", label: "ALPES-DE-HTE-PROVENCE" },
        { value: "005", label: "HAUTES-ALPES" },
        { value: "006", label: "ALPES-MARITIMES" },
        { value: "007", label: "ARDECHE" },
        { value: "008", label: "ARDENNES" },
        { value: "009", label: "ARIEGE" },
        { value: "010", label: "AUBE" },
        { value: "011", label: "AUDE" },
        { value: "012", label: "AVEYRON" },
        { value: "013", label: "BOUCHES-DU-RHONE" },
        { value: "014", label: "CALVADOS" },
        { value: "015", label: "CANTAL" },
        { value: "016", label: "CHARENTE" },
        { value: "017", label: "CHARENTE-MARITIME" },
        { value: "018", label: "CHER" },
        { value: "019", label: "CORREZE" },
        { value: "021", label: "COTE D'OR" },
        { value: "022", label: "COTES D'ARMOR" },
        { value: "023", label: "CREUSE" },
        { value: "024", label: "DORDOGNE" },
        { value: "025", label: "DOUBS" },
        { value: "026", label: "DROME" },
        { value: "027", label: "EURE" },
        { value: "028", label: "EURE-ET-LOIR" },
        { value: "029", label: "FINISTERE" },
        { value: "02A", label: "CORSE-DU-SUD" },
        { value: "02B", label: "HAUTE-CORSE" },
        { value: "030", label: "GARD" },
        { value: "031", label: "HAUTE-GARONNE" },
        { value: "032", label: "GERS" },
        { value: "033", label: "GIRONDE" },
        { value: "034", label: "HERAULT" },
        { value: "035", label: "ILLE-ET-VILAINE" },
        { value: "036", label: "INDRE" },
        { value: "037", label: "INDRE-ET-LOIRE" },
        { value: "038", label: "ISERE" },
        { value: "039", label: "JURA" },
        { value: "040", label: "LANDES" },
        { value: "041", label: "LOIR-ET-CHER" },
        { value: "042", label: "LOIRE" },
        { value: "043", label: "HAUTE-LOIRE" },
        { value: "044", label: "LOIRE-ATLANTIQUE" },
        { value: "045", label: "LOIRET" },
        { value: "046", label: "LOT" },
        { value: "047", label: "LOT-ET-GARONNE" },
        { value: "048", label: "LOZERE" },
        { value: "049", label: "MAINE-ET-LOIRE" },
        { value: "050", label: "MANCHE" },
        { value: "051", label: "MARNE" },
        { value: "052", label: "HAUTE-MARNE" },
        { value: "053", label: "MAYENNE" },
        { value: "054", label: "MEURTHE-ET-MOSELLE" },
        { value: "055", label: "MEUSE" },
        { value: "056", label: "MORBIHAN" },
        { value: "057", label: "MOSELLE" },
        { value: "058", label: "NIEVRE" },
        { value: "059", label: "NORD" },
        { value: "060", label: "OISE" },
        { value: "061", label: "ORNE" },
        { value: "062", label: "PAS-DE-CALAIS" },
        { value: "063", label: "PUY-DE-DOME" },
        { value: "064", label: "PYRENEES-ATLANTIQUES" },
        { value: "065", label: "HAUTES-PYRENEES" },
        { value: "066", label: "PYRENEES-ORIENTALES" },
        { value: "067", label: "BAS-RHIN" },
        { value: "068", label: "HAUT-RHIN" },
        { value: "069", label: "RHONE" },
        { value: "070", label: "HAUTE-SAONE" },
        { value: "071", label: "SAONE-ET-LOIRE" },
        { value: "072", label: "SARTHE" },
        { value: "073", label: "SAVOIE" },
        { value: "074", label: "HAUTE SAVOIE" },
        { value: "075", label: "PARIS" },
        { value: "076", label: "SEINE MARITIME" },
        { value: "077", label: "SEINE-ET-MARNE" },
        { value: "078", label: "YVELINES" },
        { value: "079", label: "DEUX-SEVRES" },
        { value: "080", label: "SOMME" },
        { value: "081", label: "TARN" },
        { value: "082", label: "TARN-ET-GARONNE" },
        { value: "083", label: "VAR" },
        { value: "084", label: "VAUCLUSE" },
        { value: "085", label: "VENDEE" },
        { value: "086", label: "VIENNE" },
        { value: "087", label: "HAUTE-VIENNE" },
        { value: "088", label: "VOSGES" },
        { value: "089", label: "YONNE" },
        { value: "090", label: "TERRITOIRE DE BELFORT" },
        { value: "091", label: "ESSONNE" },
        { value: "092", label: "HAUTS-DE-SEINE" },
        { value: "093", label: "SEINE-SAINT-DENIS" },
        { value: "094", label: "VAL-DE-MARNE" },
        { value: "095", label: "VAL-D'OISE" },
        { value: "971", label: "GUADELOUPE" },
        { value: "972", label: "MARTINIQUE" },
        { value: "973", label: "GUYANE" },
        { value: "974", label: "LA REUNION" },
        { value: "975", label: "SAINT-PIERRE-ET-MIQUELON" },
        { value: "976", label: "MAYOTTE" }
    ];


    const dep = depData.sort((a, b) => a.label.localeCompare(b.label));

    return (
        <form onSubmit={handleSubmit} className='form-search-college'>
            <SelectField label="Département" name="dep" dataSelect={dep} selected={dataForPlaceholder ? dataForPlaceholder[0].dep : ""} />
            <TextField label="Nom de la ville" name="city" placeholder="" defaultValue={dataForPlaceholder ? dataForPlaceholder[0].city : ""} />
            <Button type="main" children="Rechercher" />
        </form>
    );
};

export default FormSearchCollege;
