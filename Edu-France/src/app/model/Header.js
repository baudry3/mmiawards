'use client';

import React from 'react';

import Image from "next/image";
import Link from "next/link";

import { useState, useEffect } from "react";

import './../assets/style/model/Header.scss';

const Header = ({ baseline, children }) => {
    const [imageUrl, setImageUrl] = useState("/images/logo.svg");

    useEffect(() => {
        const detectPreferredTheme = () => {
            if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                setImageUrl("/images/logo-dark.svg");
            } else {
                setImageUrl("/images/logo.svg");
            }
        };

        detectPreferredTheme();

        const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
        const handleChange = () => {
            detectPreferredTheme();
        };
        mediaQuery.addListener(handleChange);

        return () => {
            mediaQuery.removeListener(handleChange);
        };
    }, []);


    return (
        <header>
            <Link href="/"><Image src={imageUrl} alt="logo" width={63} height={66} style={{ width: "auto", height: "auto" }} /></Link>
            <div>
                <h2>Édu-France</h2>
                <p>{baseline}</p>
            </div>
        </header>
    );
};

export default Header;
