import React from 'react';

import './../assets/style/model/FileAriane.scss';

import Lien from './../components/Lien';

import Image from "next/image";

const FileAriane = ({ data, children }) => {
    return (
        <nav className='file_ariane'>
            {data.map((item, index) => {
                return (
                    <div key={index}>
                        <Lien href={item.href}  type={index !== data.length - 1 ? 'third' : 'secondary'}>{item.children}</Lien>
                        {index !== data.length - 1 && <Image key={index} src="/images/darck-grey-arrow.svg" alt="arrow" width={16} height={16} style={{ width: "auto", height: "auto" }}/>}
                    </div>
                )
            })}
        </nav>
    );
};

export default FileAriane;
