import React from 'react';

import './../assets/style/model/DataTable.scss';

const DataTable = ({ datas, children }) => {
    return (
        <div className='dataTable'>
            {datas.map((data, index) => (
                <div key={index}>
                    <p>{data.number}</p>
                    <span>{data.description}</span>
                </div>
            ))}
        </div>
    );
};

export default DataTable;
