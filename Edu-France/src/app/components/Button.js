import React from 'react';

import './../assets/style/components/Button.scss';

const Button = ({ functionOnClick, type, children }) => {
  return (
    <button className={`custom-button ${type}-btn`} onClick={functionOnClick}>
      {children}
    </button>
  );
};

export default Button;
