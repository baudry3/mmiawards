import React from 'react';

const SelectField = ({ name, placeholder, label, dataSelect, selected, onChange, children }) => {
    return (
        <div className='field'>
            <label htmlFor={name}>{label}</label>
            <select id={name} name={name} placeholder={placeholder} defaultValue={selected} onChange={onChange} required>
                <option value="">{placeholder}</option>
                {dataSelect.map((option, index) => (
                    <option key={index} value={option.value}>{option.label}</option>
                ))}
            </select>
            {children}
        </div>
    );
};

export default SelectField;
