import React from 'react';


import Image from "next/image";
import './../assets/style/includes/CardInfo.scss';

const CardInfo = ({ href, headTitle, title, children }) => {
    const openLink = () => {
        window.open(href, '_blank');
    };
    return (
        <div className='CardInfo' onClick={openLink}>
            <div className='content'>
                <p>{headTitle}</p>
                <h3>{title}</h3>
            </div>
            <Image src="/images/white-arrow.svg" alt="arrow-right" width={24} height={24} style={{ width: "auto", height: "auto" }}/>
        </div>
    );
};

export default CardInfo;
