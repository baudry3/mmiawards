import React from 'react';

import './../assets/style/components/Lien.scss';
import Link from 'next/link';

const Lien = ({ href, type, children, openInBlank = false }) => {
  return (
    <Link href={href} className={`custom-link ${type}-link`} target={openInBlank ? "_blank" : ""}>
        {children}
    </Link>    
  );
};

export default Lien;
