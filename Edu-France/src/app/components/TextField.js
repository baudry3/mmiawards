import React from 'react';

import './../assets/style/includes/Field.scss';

const TextField = ({ name, placeholder, defaultValue, label, onChange, children }) => {
    return (
        <div className='field'>
            <label htmlFor={name}>{label}</label>
            <input type="text" name={name} id={name} placeholder={placeholder} defaultValue={defaultValue} onChange={onChange} required />
        </div>
    );
};

export default TextField;
